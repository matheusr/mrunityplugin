#pragma strict

private var plugin : MRPluginImport;

private var coordinates: float[] = [0.0,0.0,0.0];
private var quaternion: float[] = [0.0,0.0,0.0,0.0];
private var buttons: int[] = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
private var button: int;

/**
 * TEST Functions
 */
function PrintPressedButtons(buttons:int[])
{
	var output: String = "";
	var i : int = 0;

	for(i=0; buttons[i]>0; ++i)
		output += buttons[i] + " ";
		
	Debug.Log(output);
}

function HasButtonPressed(buttons:int[], code:int)
{
	for(var i=0; buttons[i]>0; ++i)
		if(buttons[i] == code) return true;
	return false;
}

/*END TEST Functions*/


/**
 * Creates the connection with the device using its name and...
 */
function Start () {
	plugin = new MRPluginImport("interd2");
	plugin.SetWorldReference(-200,-5,-200); //se the initial position
	
}

/**
 * Gets device information and apply to the current object.
 */
function Update () {

	//Get Pressed Buttons
	plugin.GetSeparatedPressedButtons(buttons);
	PrintPressedButtons(buttons);

	//Get Spatial Information
	plugin.GetSpatialInfo(coordinates,quaternion);
	transform.position.x = coordinates[0];
	//transform.position.y = coordinates[1]; //let Unity say where is the floor.
	transform.position.z = coordinates[2];
	Debug.Log(transform.position);
	
	//ROTATION
	//var quat : Quaternion = new Quaternion(quaternion[1], quaternion[2], quaternion[3], quaternion[0]);
	//transform.Rotate(quat.eulerAngles,Space.World);
	transform.rotation.x = quaternion[1];
	transform.rotation.y = quaternion[2];
	transform.rotation.z = quaternion[3];
	transform.rotation.w = quaternion[0];
	
	
	//using buttons to translate and rotate, consult manual to check button codes
	if(HasButtonPressed(buttons,2))
		plugin.TranslateOffset(0.1,0,0);
	if(HasButtonPressed(buttons,1))
		plugin.TranslateOffset(0,0,0.1);
	if(HasButtonPressed(buttons,4))
		plugin.TranslateOffset(-0.1,0,0);
	if(HasButtonPressed(buttons,8))
		plugin.TranslateOffset(0,0,-0.1);
	if(HasButtonPressed(buttons,16))
		plugin.RotateDegrees(0,1,0);


}