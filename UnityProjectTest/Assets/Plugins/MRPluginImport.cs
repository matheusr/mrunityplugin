using UnityEngine;
using System.Collections;
using System;
using System.Runtime.InteropServices;

/**
 * Class imports plugin functions. 
 * 
 * The DLL file must be in the same directory.
 * 
 */
public class MRPluginImport{

	/*DLL Function Import*/
	[DllImport ("MRUnityPlugin")]
	public static extern int ConnectDevice(string device_name);
	[DllImport ("MRUnityPlugin")]
	public static extern int DisconnectDevice();

	[DllImport ("MRUnityPlugin")]
	public static extern int GetSpatialInfo(float[] coord, float[] quat);
	
	[DllImport ("MRUnityPlugin")]
	public static extern void SetWorldReference(float x, float y, float z);
	[DllImport ("MRUnityPlugin")]
	public static extern void OffsetWorldReference(float dx, float dy, float dz);
	[DllImport ("MRUnityPlugin")]
	public static extern void SetLocalReference(float x, float y, float z);
	[DllImport ("MRUnityPlugin")]
	public static extern void OffsetLocalReference(float dx, float dy, float dz);
	[DllImport ("MRUnityPlugin")]
	public static extern void Translate(float x, float y, float z);
	[DllImport ("MRUnityPlugin")]
	public static extern void TranslateOffset(float dx, float dy, float dz);
	
	[DllImport ("MRUnityPlugin")]
	public static extern void Rotate(float rx, float ry, float rz);
	[DllImport ("MRUnityPlugin")]
	public static extern void RotateDegrees(float tx, float ty, float tz);
	[DllImport ("MRUnityPlugin")]
	public static extern void RotateQuaternion(float[] quaternion);
	[DllImport ("MRUnityPlugin")]
	public static extern void ResetRotation();
	
	[DllImport ("MRUnityPlugin")]
	public static extern int GetPressedButton();
	[DllImport ("MRUnityPlugin")]
	public static extern void SeparatePressedButtons(int pressed_buttons, int[] buttons);
	[DllImport ("MRUnityPlugin")]
	public static extern void GetSeparatedPressedButtons(int[] buttons);
	
	/*-------------------*/
	
	public MRPluginImport(string device_name)
	{
		ConnectDevice (device_name);
	}
	
	~MRPluginImport()
	{
		DisconnectDevice();	
	}
	
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
}
