// MRUnityPlugin.cpp : Defines the exported functions for the DLL application.
//
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <WinSock.h>
#define MRStorageType
#include <MR/Trkclient.h>
#include <MR/TRK.H>
#include <MR/MR.H>
#include <iostream>
#include "MRUnityPlugin.h"
#include <random>
#include <cmath>

using namespace std;

/**
 * Retrieves the struct isodata with connection information.
 * The data is stored on the global variable struct isodata idp*.
 *
 * @param device_name: the name of the device to be connected, should be on the file ervertab.txt
 *
 * @return Returns MRCONNECTION_OK if the connection was successful, MRCONNECTION_ERROR otherwise
 */
int ConnectDevice(char* device)
{
	idp = NULL;
	struct isodata** idp_addr = &idp;
	device_name = device;
	*idp_addr = iso_get_service((char *) device_name.c_str());
	if(idp == NULL) return MRCONNECTION_ERROR;

	read_workspace();
	set_room_reference(0,0,0); //reset room reference
	ResetRotation();

	iso_set_status(idp, ISOSTAT_CONTIN | ISOSTAT_METRIC); /*define how the information will be retrieved - Continuous and Metric format*/
	iso_set_outlist(idp, ISOOUT_XYZ | ISOOUT_QUAT | ISOOUT_BUTTONS); /*define what information will be retrieved - XYZ coordinartes, Quaternion(device rotation) and buttons information*/

	return MRCONNECTION_OK;
}

/**
 * Disconnects the device and frees the struct idp.
 */
void DisconnectDevice()
{
	iso_quit(idp);
	idp = NULL;
}

/**
 * Set the world reference.
 *
 * @param x: x coordinate.
 * @param y: y coordinate.
 * @param z: z coordinate.
 */
void SetWorldReference(scalar x, scalar y, scalar z)
{
	set_room_reference(x,y,x);
}

/**
 * Set an offset based on the world reference.
 * As I understood, all the world is moving, the old world reference remains in the same position.
 * This is, if the reference is the coordinate (0,0,0) and a offset of 10 is called in the x axis the new world coordinate reference will be (-10,0,0).
 *
 * @param dx: offset in the x axis.
 * @param dy: offset in the y axis.
 * @param dz: offset in the z axis.
 */
void OffsetWorldReference(scalar dx, scalar dy, scalar dz)
{
	offset_room_reference(dx,dy,dz);
}

/**
 * Set the local reference.
 * The local reference is based on the world reference.
 * This is if the world coordinate is (200,200,200) and the local reference is (10,10,10) the local reference in world coordinates will be (210,210,210).
 * !IMPORTANT The result of calling this method does not remain in the environment matrix.
 *
 * @param x: local x coordinate.
 * @param y: local y coordinate.
 * @param z: local z coordinate.
 */
void SetLocalReference(scalar x, scalar y, scalar z)
{
	map_reference_to(x,y,z);
}

/**
 * Set an offset based on the local reference.
 * It moves the local reference in the determined offset.
 * This is, if the local reference is the coordinates (10,10,10) and a local offset of 10 is called in the x axis the new local coordinate reference will be (20,10,10).
 * !IMPORTANT The result of calling this method does not remain in the environment matrix.
 *
 * @param dx: local offset in the x axis.
 * @param dy: local offset in the y axis.
 * @param dz: local offset in the z axis.
 */
void OffsetLocalReference(scalar dx, scalar dy, scalar dz)
{
	offset_reference(dx,dy,dz);
}

/**
 * Retrieves spatial and rotation information from the device and adjust to the application needs.
 *
 * @param coordinates: Array of floats where the first three positions are the x,y,z coordinates.
 * @param quaternion: Array of floats of 4 positions containing the quartenion of the rotation.
 *
 * @return: MRDEVICE_OK if a new information was sucessfull retrieved from the device, MRDEVICE_NOINFO if there is no new information in the device and MRDEVICE_ERROR if some error occurs.
 *
 * TODO: Multiply matrix to simplify code.
 */
int GetSpatialInfo(scalar* coordinates, quaternion* quaternion){
	
	matrix mdev[4][4], mresult[4][4], menv[4][4], mrot[4][4], mtrans[4][4];
	matrix unity_correction[4][4] = {	0,	0,	-1,	0,
										1,	0,	0,	0,
										0,	1,	0,	0,
										0,	0,	0,	1}; //matrix to convert to unity coordinate system

	int ret = iso_get_reply_nblk(idp); //non-blocking device information retrievement

	switch(ret)
	{
	case 0: //no error

		//Get Device coordinates
		coordinates[0] = idp->xpos; //get x,y,z device coordinates
		coordinates[1] = idp->ypos;
		coordinates[2] = idp->zpos;

		//CORRECTING COORDINATE BASIS
		get_mapping_matrix((char *) device_name.c_str(),mdev); //matrix got based on the workspace.txt file
		mxm4(mdev, unity_correction, mresult); //map the coordinates to the correct axis and directions
		
		//ROTATION -> HERE SHOULD MULTIPLY idp->quat to get device rotation
		 m3tom4(idp->mat,mrot);
		 mxm4(mresult,mrot,mresult);
		 m4toq(mrot,quaternion);
		 //printm4(mrot);

		//TRANSLATION
		get_environment_matrix(menv);
		mtrans4(menv,mtrans);
		//mxm4(mresult,mtrans,mresult);

		//printcoord(coordinates);
		vtransform4(mresult, &coordinates[0], &coordinates[1], &coordinates[2], 1);
		vtransform4(mtrans, &coordinates[0], &coordinates[1], &coordinates[2], 1);
		//printcoord(coordinates);

		return MRDEVICE_OK;
		break;
	case -1: //error
		cerr << "Error retrieving device information" << endl;
		return MRDEVICE_ERROR;
		break;
	case -2: //no information
		cerr << "No information avaiable" << endl;
		return MRDEVICE_NOINFO;
		break;
	}

	return MRDEVICE_ERROR;
}

/**
 * Get the summed values of the pressed buttons from the device, all buttons have a differenc 2 potency value.
 *
 * @return Summed value of the pressed controller buttons.
 */
int GetPressedButton()
{
	int ret = iso_get_reply_nblk(idp); //non-blocking device information retrievement

	switch(ret)
	{
	case 0: //no error
		return idp->buttons;
	break;

	case -1: //error
		cerr << "Error retrieving device information" << endl;
		return MRDEVICE_ERROR;
	break;

	case -2: //no information
		cerr << "No information avaiable" << endl;
		return MRDEVICE_NOINFO;
	break;
	}

}

/**
 * Separate the summed values of the buttons, given it separately.
 *
 * @param pressed_buttons: the summed buttons' value returned by the controller.
 * @param buttons: array of integers that will receive the pressed button sorted in decrescent order, a -1 value show the end of the array.
 *
 * !IMPORTANT The buttons array should have, at least, the number of buttons in the controller plus one.
 */
void SeparatePressedButtons(int pressed_buttons, int* buttons)
{
	int max_button_value = 1024; //10 button -> 2^10= 1024
	int i = 0;

	cout << pressed_buttons << endl;

	while(pressed_buttons > 0)
	{
		if(pressed_buttons >= max_button_value)
		{
			pressed_buttons -= max_button_value;
			buttons[i++] = max_button_value;
		}
		max_button_value /= 2;
	}

	buttons[i] = -1; //the end of the array

}

/**
 * Get each pressed button value separately.
 *
 * @param buttons: array of integers that will receive the pressed button sorted in decrescent order, a -1 value show the end of the array.
 *
 * !IMPORTANT The buttons array should have, at least, the number of buttons in the controller plus one.
 */
void GetSeparatedPressedButtons(int* buttons)
{
	SeparatePressedButtons(GetPressedButton(), buttons);
}

/**
 * Rotate the coordinate system.
 * Just call the Rotate function converting degrees to radians.
 *
 * @param tx: angle to rotate x axis in degrees.
 * @param ty: angle to rotate y axis in degrees.
 * @param tz: angle to rotate z axis in degrees.
 *
 */
void RotateDegrees(scalar tx, scalar ty, scalar tz)
{
	Rotate(DegreeToRad(tx),DegreeToRad(ty),DegreeToRad(tz));
}

/**
 * Rotate the coordinate system.
 * The rotation is cumulative, this is, it will be applied in the current rotation.
 *
 * @param rx: angle to rotate x axis in radians.
 * @param ry: angle to rotate y axis in radians.
 * @param rz: angle to rotate z axis in radians.
 *
 */
void Rotate(scalar rx, scalar ry, scalar rz)
{
	matrix rot_x[3][3] = {	1,	0,			0,
							0,	cos(rx),	-sin(rx),	
							0,	sin(rx),	cos(rx)};
	matrix rot_y[3][3] = {	cos(ry),	0,	sin(ry),
							0,			1,	0,	
							-sin(ry),	0,	cos(ry)};
	matrix rot_z[3][3] = {	cos(rz),	-sin(rz),	0,
							sin(rz),	cos(rz),	0,	
							0,			0,			1};

	mxm3(idp->mat, rot_x, idp->mat);
	mxm3(idp->mat, rot_y, idp->mat);
	mxm3(idp->mat, rot_z, idp->mat);
}

/**
 * Rotate the coordinate system.
 * The rotation is not cumulative, this is, the quaternion rotation will be the only applied to the coordinate system.
 *
 * @param quaternion: quaternion containing the rotation info.
 *
 */
void RotateQuaternion(quaternion* quaternion)
{
	qtom3(quaternion,idp->mat);
}

/**
 * Reset the rotation matrix.
 */
void ResetRotation()
{
	munit3(idp->mat);
}

/**
 * Translate the origin to the defined point.
 *
 * @param x: x coordinate.
 * @param y: y coordinate.
 * @param z: z coordinate.
 *
 */
void Translate(scalar x, scalar y, scalar z)
{
	SetWorldReference(-x,-y,-z);
}

/**
 * Sum the desired amount of space to the current position.
 *
 * @param dx: amount to sum in the x coordinate. 
 * @param dy: amount to sum in the y coordinate.
 * @param dz: amount to sum in the z coordinate.
 *
 */
void TranslateOffset(scalar dx, scalar dy, scalar dz)
{
	OffsetWorldReference(-dx,-dy,-dz);
}


/* INTERN FUNCTIONS */

/**
 * Convert degrees to radians.
 */
scalar DegreeToRad(scalar degree)
{
	return degree*PI/180;
}

/**
 * Just copies the content from a quaternion to another one.
 *
 * @param qfrom: quaternion which will have content copied.
 * @param qto: quaternion which will receieve the copied content.
 *
 */
void qtoq(quaternion* qfrom, quaternion* qto)
{
	for(int i=0; i<4; ++i)
		qto[i] = qfrom[i];
}

void vtov3(scalar* vfrom, scalar* vto)
{
	for(int i=0; i<3; ++i)
		vto[i] = vfrom[i];
}

/**
 * Simulates an x,y,z and rotation motion. It is used only for test purposes (maybe when the device is not working).
 *
 * @param coordinates: Array of floats where the first three positions are the x,y,z coordinates.
 * @param quaternion: Array of floats of 4 positions containing the quartenion of the rotation.
 *
 */
void SimulateMotion(scalar* coordinates, quaternion* quaternion)
{	
	std::random_device rd;

	int prob_fwd_bck = 2; /*probabilty to go more foward than backwards - MUST be positive to work correctly!*/

	float x0 = 0.0, y0 = 0.0, z0= 0.0;
	float q0 = 0.0, q1 = 0.0, q2 = 0.0, q3 = 0.0;

	coordinates[0] += x0 + (((float)(rd()%((prob_fwd_bck+1)*100)))-100)/100;
	coordinates[1] += y0 + (((float)(rd()%((prob_fwd_bck+1)*100)))-100)/100;
	coordinates[2] += z0 + (((float)(rd()%((prob_fwd_bck+1)*100)))-100)/100;

	quaternion[0] += q0;
	quaternion[1] += q1;
	quaternion[2] += q2;
	quaternion[3] += q3;
}

void printm4(matrix m[4][4])
{
	cout << "MATRIX PRINT" << endl;
	for(int i=0; i<4; ++i)
	{
		for(int j=0; j<4; ++j)
			cout << m[i][j] << " ";
		cout << endl;
	}
	cout << endl;
}

void printm3(matrix m[3][3])
{
	cout << "MATRIX PRINT" << endl;
	for(int i=0; i<3; ++i)
	{
		for(int j=0; j<3; ++j)
			cout << m[i][j] << " ";
		cout << endl;
	}
	cout << endl;
}

void printq(quaternion q[4])
{
	cout << "QUATERNION PRINT: ";
	cout << q[0] << " " << q[1] << "i " << q[2] << "j " << q[3] << "k ";
	cout << endl << endl;
}

void printv3(scalar v[3])
{
	for(int i=0; i<3; ++i)
		cout << v[i] << " ";
	cout << endl << endl;
}

void printe(angles e[3])
{
	cout << "EULER ANGLES PRINT: ";
	printv3(e);
}

void printcoord(scalar coord[3])
{
	cout << "COORDINATES PRINT: ";
	printv3(coord);
}