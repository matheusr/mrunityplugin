#ifdef MRUNITYPLUGIN_EXPORTS
#define MRUNITYPLUGIN_API __declspec(dllexport)
#else
#define MRUNITYPLUGIN_API __declspec(dllimport)
#endif

#include <windows.h>
#define MRStorageType
#include <MR/Trkclient.h>
#include <MR/TRK.H>
#include <MR/MR.H>

using namespace std;

#define MRCONNECTION_ERROR -1
#define MRCONNECTION_OK 1
#define MRDEVICE_OK 0
#define MRDEVICE_ERROR -1
#define MRDEVICE_NOINFO -2

#define PI 3.14159

FILE* dbgfile;

struct isodata* idp;
string device_name;

/* EXTERN FUNCTIONS */
extern "C" int MRUNITYPLUGIN_API ConnectDevice(char* device);
extern "C" void MRUNITYPLUGIN_API DisconnectDevice();

extern "C" int MRUNITYPLUGIN_API GetSpatialInfo(scalar* coordinates, quaternion* quaternion);

extern "C" void MRUNITYPLUGIN_API SetWorldReference(scalar x, scalar y, scalar z);
extern "C" void MRUNITYPLUGIN_API OffsetWorldReference(scalar dx, scalar dy, scalar dz);
extern "C" void MRUNITYPLUGIN_API SetLocalReference(scalar x, scalar y, scalar z);
extern "C" void MRUNITYPLUGIN_API OffsetLocalReference(scalar dx, scalar dy, scalar dz);
extern "C" void MRUNITYPLUGIN_API Translate(scalar x, scalar y, scalar z);
extern "C" void MRUNITYPLUGIN_API TranslateOffset(scalar dx, scalar dy, scalar dz);

extern "C" int MRUNITYPLUGIN_API GetPressedButton();
extern "C" void MRUNITYPLUGIN_API SeparatePressedButtons(int pressed_buttons, int* buttons);
extern "C" void MRUNITYPLUGIN_API GetSeparatedPressedButtons(int* buttons);

extern "C"void MRUNITYPLUGIN_API Rotate(scalar rx, scalar ry, scalar rz);
extern "C" void MRUNITYPLUGIN_API RotateDegrees(scalar tx, scalar ty, scalar tz);
extern "C" void MRUNITYPLUGIN_API RotateQuaternion(quaternion* quaternion);
extern "C" void MRUNITYPLUGIN_API ResetRotation();



/* INTERN FUNCTIONS */
scalar DegreeToRad(scalar degree);
void qtoq(quaternion* qfrom, quaternion* qto);
void SimulateMotion(scalar* coordinates, quaternion* quaternion);
void printm4(matrix m[4][4]);
void printm3(matrix m[3][3]);
void printq(quaternion q[4]);
void printe(angles e[3]);
void printcoord(scalar coord[3]);
