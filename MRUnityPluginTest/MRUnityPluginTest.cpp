// MRUnityPluginTest.cpp : Defines the entry point for the console application.
//

#include <cstdlib>
#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <iostream>
#define MRStorageType
#include <MR/Trkclient.h>
#include <MR/TRK.H>
#include <MR/MR.H>
#include "MRUnityPlugin.h"

using namespace std;

#define TWOWAY

int _tmain(int argc, _TCHAR* argv[])
{
	int button = 0;
	int buttons[12];

	if(ConnectDevice("interd2") == MRCONNECTION_ERROR)
	{
		cout << "Error connecting to device\n";
		system("PAUSE");
		exit(EXIT_FAILURE);
	}

	float coords[3] = {0,0,0};
	float quat[4] = {0,0,0,0};
	
	for(int i=0; i<100; i++)
	{
		GetSpatialInfo(coords,quat);
		cout << coords[0] << " " << coords[1] << " " << coords[2] << endl;

		//BUTTON TEST
		GetSeparatedPressedButtons(buttons);
		cout << buttons[0] << " ";
		for(int j=0; buttons[j]!=-1; ++j)
			cout << buttons[j] << "  ";
		cout << endl;

		sleep(1);

	}

	DisconnectDevice();
	system("PAUSE");
	return EXIT_SUCCESS;
}

